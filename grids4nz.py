#! /usr/bin/env python3

"""
Proof of concept - It is fast to make a table of all NZ s2_12_ids and their
centroid lat/lons (on local db if not over network to Greenplum given current
approach of adding one row at a time ;-) (easy enough to do in batches using
fetchmany and multi-line inserts when productionised)) and very fast to filter
them by centroid lat/lons using polygon coordinates. Not sure yet about shape
files and geoms although the latter should be easy (perhaps ask Ivan or
Johannes).

RTOs

 Area Outside RTO
 Auckland RTO
 Bay of Plenty RTO
 Central Otago RTO
 Christchurch
 Clutha
 Coromandel RTO
 Dunedin RTO
 Fiordland RTO
 Gisborne RTO
 Hawkes Bay RTO
 Kapiti-Horowhenua RTO
 Kawerau-Whakatane
 Lake Taupo RTO
 Lake Wanaka RTO
 Manawatu RTO
 Marlborough RTO
 Nelson Tasman RTO
 North Canterbury
 Northland RTO
 Queenstown RTO
 Rotorua RTO
 Ruapehu RTO
 South Canterbury
 Southland RTO
 Taranaki RTO
 Waikato RTO
 Wairarapa RTO
 Waitaki RTO
 Wanganui RTO
 Wellington RTO
 West Coast RTO

"""

from collections import namedtuple
from pprint import pprint as pp

import s2sphere as s2

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import (area, conf, dates, db, find_relocations as findrel,
    folium_map, location_sources as locsrcs, utils)

Dates = dates.Dates

S2_dets = namedtuple('S2_dets', 's2_12_id, lat, lon') 

## bounding boxes bottom-left to top-right
north_island_bb = ((-41.641982, 172.508895), (-33.856788, 179.005234)) 
south_island_bb = ((-47.368870, 165.677994), (-40.440918, 174.711721)) 
nz_bounding_boxes = [
    north_island_bb,
    south_island_bb,
]
isthmus_bounding_boxes = [((-36.931799, 174.701858), (-36.843880, 174.880003))]
is_t = -36.843880
is_b = -36.931799
is_l = 174.701858
is_r = 174.880003
isthmus_polygon_coords = [
    (is_t, is_l),
    (is_t, is_r),
    (is_b, is_r),
    (is_b, is_l),
    (is_t, is_l),
]
ma_t = -36.87  ## guessed when no access to map and internet
ma_b = -36.91
ma_l = 174.71
ma_r = 174.74
mt_albert_polygon_coords = [
    (ma_t, ma_l),
    (ma_t, ma_r),
    (ma_b, ma_r),
    (ma_b, ma_l),
    (ma_t, ma_l),
]
nth_bottom_coord_0 = (-36.461051, 173.982547)
nth_bottom_coord_1 = (-36.400530, 174.198887)
nth_bottom_coord_2 = (-36.432070, 174.362049)
nth_bottom_coord_3 = (-36.038829, 174.666107)
nth_right_coord_0 = (-36.038829, 174.666107)
nth_right_coord_1 = (-35.100640, 174.438780)
nth_top_coord_0 = (-34.310826, 173.125310)
nth_top_coord_1 = (-34.269702, 172.078246)
northland_polygon_coords = [
    nth_bottom_coord_0,
    nth_bottom_coord_1,
    nth_bottom_coord_2,
    nth_bottom_coord_3,
    nth_right_coord_0,
    nth_right_coord_1,
    nth_top_coord_0,
    nth_top_coord_1,
    nth_bottom_coord_0,  ## repeated to make join
]

def get_cellids(bounding_boxes):
    debug = False
    cellids = set()
    for bb in bounding_boxes:
        bottom_left, top_right = bb 
        rect = s2.sphere.LatLngRect(
            s2.sphere.LatLng.from_degrees(*bottom_left),
            s2.sphere.LatLng.from_degrees(*top_right))
        coverer = s2.sphere.RegionCoverer()
        coverer.min_level = 12
        coverer.max_level = 12
        bb_cellids = coverer.get_covering(rect)
        if debug: print(len(bb_cellids))
        cellids = cellids.union(set(bb_cellids))
    if debug: print(len(cellids))
    return cellids

def get_s2_12_id_coords(cellids):
    """
    Get coordinates for centroids. In named tuple.
    """
    debug = False
    S2s_dets = []
    for cellid in cellids:
        if debug: print('cellid', cellid)
        cell = s2.sphere.Cell(cellid)
        s2_12_id = cellid.parent(12).id()
        centre = cell.get_center()
        centroid_coord = s2.sphere.LatLng.from_point(centre)
        lat = centroid_coord.lat().degrees
        lon = centroid_coord.lng().degrees
        if debug: print(lat, lon)
        S2s_dets.append(S2_dets(s2_12_id, lat, lon))
    if debug: print(S2s_dets)
    return S2s_dets

def make_id_coords_tbl(con_rem, cur_rem, bounding_boxes):
    debug = False
    cellids = get_cellids(bounding_boxes)
    S2s_dets = get_s2_12_id_coords(cellids)
    if debug: pp(S2s_dets[:100])
    db.Pg.drop_tbl(con_rem, cur_rem, conf.S2_COORDS_GREENPLUM)
    sql_make_tbl = """\
    CREATE TABLE {s2_coords} (
      s2_12_id numeric(22, 0),
      lat double precision,
      lon double precision,
      primary key (s2_12_id)
    )
    """.format(s2_coords=conf.S2_COORDS_GREENPLUM)
    cur_rem.execute(sql_make_tbl)
    con_rem.commit()
    sql_insert_tpl = """\
    INSERT INTO {s2_coords}
    (s2_12_id, lat, lon)
    VALUES (%s, %s, %s)
    """.format(s2_coords=conf.S2_COORDS_GREENPLUM)
    for n, S2_dets in enumerate(S2s_dets, 1):
        cur_rem.execute(sql_insert_tpl, S2_dets)
        if n % 5000 == 0:
            print(utils.prog(n))
    con_rem.commit()
    ## Give public read permissions
    sql_permissions = """\
    GRANT SELECT ON TABLE {s2_coords} TO public
    """.format(s2_coords=conf.S2_COORDS_GREENPLUM)
    cur_rem.execute(sql_permissions)
    con_rem.commit()
    print("Finished!")

def get_s2_12_ids_in_polygon(cur_rem, polygon_coords, show_on_map=False,
        zoom_start=7):
    """
    geom_coords -- ending point must be same as the starting point to close up
    polygon
    """
    debug = False
    if len(polygon_coords) < 4:
        raise Exception("Not enough coordinates to define a polygon - "
            "only {}".format(len(polygon_coords)))
    if polygon_coords[0] != polygon_coords[-1]:
        raise Exception("Must ending point which is the same as the "
            "starting point to close up polygon")
    polygon_str = db.Pg.make_polygon_str(polygon_coords)
    sql = """\
    SELECT s2_12_id::text, lat, lon
    FROM (
      SELECT 
      s2_12_id,
        ST_GeomFromText(
          'POINT(' || lon || ' ' || lat || ')', {srid}
        ) AS
      s2_12_centroid_point,
      lat, lon
      FROM {s2_coords}
    ) AS points
    CROSS JOIN
    (SELECT {polygon_str} AS
      polygon
    ) AS polygon
    WHERE ST_CONTAINS(polygon, s2_12_centroid_point)
    ORDER BY s2_12_id
    """.format(s2_coords=conf.S2_COORDS_GREENPLUM, polygon_str=polygon_str,
        srid=conf.WGS84_SRID)
    cur_rem.execute(sql)
    data = cur_rem.fetchall()
    contained_ids = [int(row['s2_12_id']) for row in data]
    if show_on_map:
        coord_data = [((lat, lon), int(s2_12_id))
            for s2_12_id, lat, lon in data]
        if debug: print(coord_data)
        title = 'Correct centroids as per polygon?'
        folium_map.FoliumMap.show_centroids_on_map(title, coord_data,
            zoom_start)
    return contained_ids

def validate_results(cur_rem, rto):
    title = "Check grids for {}".format(rto)
    sql = """\
    SELECT s2_12_id, lat, lon
    FROM (
      SELECT s2_12_id
      FROM {rtos2ids}
      WHERE rto = %s
    ) AS rto_ids
    INNER JOIN
    {s2_coords}
    USING(s2_12_id)
    """.format(rtos2ids=conf.RTOS_TO_S2_12_IDS_GREENPLUM,
        s2_coords=conf.S2_COORDS_GREENPLUM)
    cur_rem.execute(sql, (rto, ))
    coord_data = [((lat, lon), s2_12_id)
        for s2_12_id, lat, lon in cur_rem.fetchall()]
    folium_map.FoliumMap.show_centroids_on_map(title, coord_data)

def overlap_poc():
    unused_con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    raw_mt_albert_coords = "new google.maps.LatLng(-36.87303,174.71824), new google.maps.LatLng(-36.87227,174.69863), new google.maps.LatLng(-36.88744,174.69344), new google.maps.LatLng(-36.90405,174.69129), new google.maps.LatLng(-36.90543,174.71996), new google.maps.LatLng(-36.90968,174.74657), new google.maps.LatLng(-36.87824,174.76562),"
    raw_auck_coords = "new google.maps.LatLng(-36.83182,174.91119), new google.maps.LatLng(-36.82282,174.82085), new google.maps.LatLng(-36.82371,174.74287), new google.maps.LatLng(-36.94303,174.72288), new google.maps.LatLng(-36.98171,175.00286), new google.maps.LatLng(-36.93438,175.02603), new google.maps.LatLng(-36.85737,175.06431),"
    polygon_coords_mt_albert = area.Area.get_coords(raw_mt_albert_coords)
    polygon_coords_auck = area.Area.get_coords(raw_auck_coords)
    polygon_str_a = db.Pg.make_polygon_str(polygon_coords_mt_albert)
    polygon_str_b = db.Pg.make_polygon_str(polygon_coords_auck)
    sql = """\
    SELECT
    region_shape,
    grid12_shape,
      100*(
        ST_Area(ST_Intersection(grid12_shape, region_shape))
        /
        ST_Area(grid12_shape)
      )::double precision AS
    pct_overlap
    FROM (SELECT
        {polygon_str_a} AS
      grid12_shape
    ) AS grids
    JOIN
    (SELECT
        {polygon_str_b} AS
      region_shape
    ) AS regions
    ON regions.region_shape && grids.grid12_shape  -- pre-filter for performance using indexed bounding boxes
    AND ST_Intersects(grid12_shape, region_shape)  -- actual intersections
    """.format(polygon_str_a=polygon_str_a, polygon_str_b=polygon_str_b)
    cur_local.execute(sql)
    print(cur_local.fetchall())

def main():
    con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    
    #locsrcs.RegionTables.nuke_replace_all_region_tables(cur_local)
    
    start_datetime_str, end_datetime_str = Dates.date_str_to_datetime_str_range('2016-03-29')
    rti = 31671
    findrel.FindRelocations.get_likely_location_dts(cur_local,
        start_datetime_str, end_datetime_str, rti)

    #folium_map.FoliumMap.map_rtis_by_coord(cur_local)

    #locsrcs.RegionTables.display_regions()
    #locsrcs.RegionTables.display_region(region_id=22, label='Taranaki RTO')
    #locsrcs.RegionTables.check_rto_grid12s([105, ])

    #locsrcs.RegionTables.check_rto_grid12s(conf.RTO_IDS, lightweight=True)
    
if __name__ == '__main__':
    
    main()
